package br.com.zallpy.controller;

import br.com.zallpy.domain.Employee;
import br.com.zallpy.domain.Project;
import br.com.zallpy.enums.EmployeeTypeEnum;
import br.com.zallpy.enums.ProjectTypeEnum;
import br.com.zallpy.mapper.EmployeeMapper;
import br.com.zallpy.request.EmployeeRequest;
import br.com.zallpy.response.EmployeeResponse;
import br.com.zallpy.service.EmployeeService;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.LocalDate;
import java.util.List;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;

@ExtendWith(SpringExtension.class)
public class EmployeeControllerTest {
    @InjectMocks
    private EmployeeController employeeController;

    @Mock
    EmployeeService employeeService;

    @Mock
    EmployeeMapper employeeMapper;

    @BeforeEach
    public void setup(){
        doNothing().when(employeeService).addWorkedHourOnClientA(any(Employee.class));
        doNothing().when(employeeService).addWorkedHourOnClientB(any(Employee.class));
        BDDMockito.when(employeeService.getProjectsOfProgrammer1(any())).thenReturn(employeeResponseBuilder());
        BDDMockito.when(employeeService.getProjectsOfProgrammer2(any())).thenReturn(employeeResponseBuilder());
        BDDMockito.when(employeeService.listAll()).thenReturn(asList(employeeBuilderAllocatedInBProject()));
    }

    @Test
    @DisplayName("Launch hour on project A")
    public void givenGenericController_whenLaunchHoursOnProjectA_thenOK(){
        ResponseEntity entity = employeeController.addWorkedHourOnClientA(EmployeeRequestBuilderAllocatedInAProject());
        assertThat(entity).isNotNull();
        assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    @DisplayName("Launch hour on project B")
    public void givenGenericController_whenLaunchHoursOnProjectB_thenOK(){
        ResponseEntity entity = employeeController.addWorkedHourOnClientB(EmployeeRequestBuilderAllocatedInBProject());
        assertThat(entity).isNotNull();
        assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    @DisplayName("List A projects done by programmer 1")
    public void givenGenericController_listProjectAProject(){
        ResponseEntity<EmployeeResponse> projectsProgrammerOne = employeeController.getProjectsProgrammerOne("0001");
        assertThat(projectsProgrammerOne).isNotNull();
        assertThat(projectsProgrammerOne.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(projectsProgrammerOne.getBody()).isNotNull();
    }

    @Test
    @DisplayName("List A projects done by programmer 2")
    public void givenGenericController_listProjectBProject(){
        ResponseEntity<EmployeeResponse> projectsProgrammerOne = employeeController.getProjectsProgrammerTwo("0001");
        assertThat(projectsProgrammerOne).isNotNull();
        assertThat(projectsProgrammerOne.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(projectsProgrammerOne.getBody()).isNotNull();
    }

    @Test
    @DisplayName("List A projects done by programmer 2")
    public void givenGenericController_listProjectAllProjects(){
        ResponseEntity<List<Employee>> listResponseEntity = employeeController.listEmployees();
        assertThat(listResponseEntity).isNotNull();
        assertThat(listResponseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(listResponseEntity.getBody()).isNotNull();
        assertThat(listResponseEntity.getBody().size()).isEqualTo(1);
        assertThat(listResponseEntity.getBody().get(0).getName()).isEqualTo("John Due");
    }




    public EmployeeRequest EmployeeRequestBuilderAllocatedInAProject(){
        return  EmployeeRequest.builder()
                .employeeType("Programmer1")
                .name("John Due")
                .cpf("089.876.123-02")
                .rg("12.456.754-4")
                .address("Street of River, 12 - New Your, USA.")
                .birthDay(LocalDate.of(1988, 01, 01))
                .email("jd@zallpy.com")
                .workedHours(10)
                .code("0001")
                .project(
                        Project.builder()
                                .id(1L)
                                .client(ProjectTypeEnum.A)
                                .build()
                )
                .build();
    }

    public EmployeeRequest EmployeeRequestBuilderAllocatedInBProject(){
        return  EmployeeRequest.builder()
                .employeeType("Programmer1")
                .name("John Due")
                .cpf("089.876.123-02")
                .rg("12.456.754-4")
                .address("Street of River, 12 - New Your, USA.")
                .birthDay(LocalDate.of(1988, 01, 01))
                .email("jd@zallpy.com")
                .workedHours(10)
                .code("0001")
                .project(
                        Project.builder()
                                .id(1L)
                                .client(ProjectTypeEnum.B)
                                .build()
                )
                .build();
    }

    public EmployeeResponse employeeResponseBuilder(){
        return EmployeeResponse.builder()
                .projects(
                        asList(employeeBuilderAllocatedInBProject()
                        ))
                .totalWorkedHours(10)
                .build();
    }

    public Employee employeeBuilderAllocatedInBProject(){
        return  Employee.builder()
                .employeeType(EmployeeTypeEnum.Programmer2)
                .name("John Due")
                .cpf("089.876.123-02")
                .rg("12.456.754-4")
                .address("Street of River, 12 - New Your, USA.")
                .birthDay(LocalDate.of(1988, 01, 01))
                .email("jd@zallpy.com")
                .workedHours(10)
                .code("0001")
                .project(
                        Project.builder()
                                .id(1L)
                                .client(ProjectTypeEnum.B)
                                .build()
                )
                .build();
    }
}
