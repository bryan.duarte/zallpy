package br.com.zallpy.controller;

import br.com.zallpy.domain.Project;
import br.com.zallpy.enums.ProjectTypeEnum;
import br.com.zallpy.mapper.ProjectMapper;
import br.com.zallpy.request.ProjectRequest;
import br.com.zallpy.response.ProjectResponse;
import br.com.zallpy.response.ProjectTotalsResponse;
import br.com.zallpy.service.ProjectService;
import br.com.zallpy.util.DateTimeUtil;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatCode;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@Slf4j
public class ProjectControllerTest {
    @InjectMocks
    private ProjectController projectController;

    @Mock
    private ProjectService projectService;

    @Mock
    private DateTimeUtil dateTimeUtil;

    @Mock
    ProjectMapper projectMapper;

    private List<Project> projectList;

    @BeforeEach
    public void setup(){
        this.projectList = asList(projectABuilder(), projectBBuilder());
        when(projectService.add(projectABuilder())).thenReturn(ProjectResponse.builder().projectList(asList(projectABuilder())).build());
        when(projectService.listAll()).thenReturn(ProjectResponse.builder().projectList(projectList).build());
        when(projectService.findById(ArgumentMatchers.anyLong())).thenReturn(ProjectResponse.builder().projectList(asList(projectABuilder())).build());
        when(projectService.getTotalOfWorkedHoursOnProjectA()).thenReturn(projectTotalsAResponseBuilder());
        when(projectService.getTotalOfWorkedHoursOnProjectB()).thenReturn(projectTotalsAResponseBuilder());
        when(projectService.getTotalOfWorkedHoursInAllProjects()).thenReturn(projectTotalsAResponseBuilder());
        doNothing().when(projectService).update(any(ProjectRequest.class));
        doNothing().when(projectService).delete(anyLong());
        doNothing().when(projectService).activeRegisteredProjects();
    }
    @Test
    @DisplayName("Return list of projects")
    public void givenGenericController_whenListProjectWithAuthorizedUser_thenReturn(){
        ResponseEntity<ProjectResponse> projectResponseResponseEntity = projectController.listAll();
        assertThat(projectResponseResponseEntity).isNotNull();
        assertThat(projectResponseResponseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(projectResponseResponseEntity.getBody().getProjectList().size()).isEqualTo(2);
        assertThat(projectResponseResponseEntity.getBody().getProjectList().get(0).getClient()).isEqualTo(ProjectTypeEnum.A);
        assertThat(projectResponseResponseEntity.getBody().getProjectList().get(1).getClient()).isEqualTo(ProjectTypeEnum.B);
    }

    @Test
    @DisplayName("Return project by id")
    public void givenGenericController_whenFindByIdProjectWithAuthorizedUser_thenReturn(){
        ResponseEntity<ProjectResponse> projectResponseResponseEntity = projectController.findById(1L);
        assertThat(projectResponseResponseEntity).isNotNull();
        assertThat(projectResponseResponseEntity.getBody()).isEqualTo(ProjectResponse.builder().projectList(asList(projectABuilder())).build());
    }

    @Test
    @DisplayName("Save project")
    public void givenGenericController_whenSaveProjectWithAuthorizedUser_thenReturn(){
        ResponseEntity<ProjectResponse> project = projectController.add(projectRequestBuilder());
        assertThat(project.getStatusCode()).isEqualTo(HttpStatus.CREATED);
    }

    @Test
    @DisplayName("Delete project")
    public void givenGenericController_whenSaveProjectWithAuthorizedUser_thenOK(){
        ResponseEntity<ProjectResponse> project = projectController.add(projectRequestBuilder());
        assertThat(project.getStatusCode()).isEqualTo(HttpStatus.CREATED);
    }

    @Test
    @DisplayName("Update project")
    public void givenGenericController_whenUpdateProjectWithAuthorizedUser_thenOK(){
       assertThatCode(() -> projectController.update(projectRequestBuilder()))
               .doesNotThrowAnyException();
        ResponseEntity update = projectController.update(projectRequestBuilder());

        assertThat(update).isNotNull();
        assertThat(update.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    @DisplayName("Delete project")
    public void givenGenericController_whenDeleteProjectWithAuthorizedUser_thenOK(){
        assertThatCode(() -> projectController.delete(1L))
                .doesNotThrowAnyException();
        ResponseEntity delete = projectController.delete(1L);

        assertThat(delete).isNotNull();
        assertThat(delete.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    @DisplayName("Active default projects")
    public void givenGenericController_whenActiveDefaultProjectsWithAuthorizedUser_thenOK(){
        assertThatCode(() -> projectController.activeRegisteredProjects())
                .doesNotThrowAnyException();
        ResponseEntity active = projectController.update(projectRequestBuilder());

        assertThat(active).isNotNull();
        assertThat(active.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    @DisplayName("Get total of project A")
    public void givenGenericController_whenGetTotalsOfProjectAWithAuthorizedUser_thenOK(){
        ResponseEntity totalOfWorkedHoursOnProjectA = projectController.getTotalOfWorkedHoursOnProjectA();
        assertThat(totalOfWorkedHoursOnProjectA).isNotNull();
        assertThat(totalOfWorkedHoursOnProjectA.getBody()).isNotNull();

    }

    @Test
    @DisplayName("Get total of project B")
    public void givenGenericController_whenGetTotalsOfProjectBWithAuthorizedUser_thenOK(){
        ResponseEntity totalOfWorkedHoursOnProjectAB= projectController.getTotalOfWorkedHoursOnProjectB();
        assertThat(totalOfWorkedHoursOnProjectAB).isNotNull();
        assertThat(totalOfWorkedHoursOnProjectAB.getBody()).isNotNull();

    }

    @Test
    @DisplayName("Get total of all projects")
    public void givenGenericController_whenGetTotalsOfAllProjectsWithAuthorizedUser_thenOK(){
        ResponseEntity totalOfWorkedHoursOnProjectAB= projectController.getTotalOfWorkedHoursInAllProjects();
        assertThat(totalOfWorkedHoursOnProjectAB).isNotNull();
        assertThat(totalOfWorkedHoursOnProjectAB.getBody()).isNotNull();
    }

    public Project projectABuilder(){
        return Project.builder()
                .client(ProjectTypeEnum.A)
                .build();
    }

    public ProjectRequest projectRequestBuilder(){
        return ProjectRequest.builder()
                .client("A")
                .build();
    }

    public Project projectBBuilder(){
        return Project.builder()
                .id(2L)
                .client(ProjectTypeEnum.B)
                .build();
    }

    public ProjectTotalsResponse projectTotalsAResponseBuilder(){
        return ProjectTotalsResponse.builder()
                .projectType(ProjectTypeEnum.A.description)
                .totalWorkedHours(10)
                .build();
    }

    public ProjectTotalsResponse projectTotalsBResponseBuilder(){
        return ProjectTotalsResponse.builder()
                .projectType(ProjectTypeEnum.A.description)
                .totalWorkedHours(10)
                .build();
    }
}
