package br.com.zallpy.repository;
import br.com.zallpy.domain.Employee;
import br.com.zallpy.domain.Project;
import br.com.zallpy.enums.EmployeeTypeEnum;
import br.com.zallpy.enums.ProjectTypeEnum;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;

import java.time.LocalDate;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@DisplayName("EmployeeRepository Unit Tests")
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class EmployeeRepositoryTest {
    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private ProjectRepository projectRepository;

    @Test
    @DisplayName("Test to list all employees on project A by code")
    public void givenGenericRepository_whenListProjectByProgrammer1_thenReturn(){
        Employee employee = employeeBuilderAllocatedInAProject();

        Project project = projectABuilder();

        projectRepository.save(project);
        employeeRepository.save(employee);

        List<Employee> projectsOfProgrammer1 = employeeRepository.getProjectsOfProgrammer1(employee.getCode());

        assertThat(projectsOfProgrammer1).isNotNull();
        assertThat(projectsOfProgrammer1.size()).isEqualTo(1);
        assertThat(projectsOfProgrammer1.get(0).getName()).isEqualTo("John Due");
    }

    @Test
    @DisplayName("Test to list all employees on project B by code")
    public void givenGenericRepository_whenListProjectByProgrammer2_thenReturn(){
        Employee employee = employeeBuilderAllocatedInBProject();

        Project project = projectBBuilder();

        projectRepository.save(project);
        employeeRepository.save(employee);

        List<Employee> projectsOfProgrammer2 = employeeRepository.getProjectsOfProgrammer2(employee.getCode());

        assertThat(projectsOfProgrammer2).isNotNull();
        assertThat(projectsOfProgrammer2.size()).isEqualTo(1);
        assertThat(projectsOfProgrammer2.get(0).getName()).isEqualTo("John Due");
    }

    public Project projectABuilder(){
        return Project.builder()
                .id(1L)
                .client(ProjectTypeEnum.A)
                .build();
    }

    public Project projectBBuilder(){
        return Project.builder()
                .id(2L)
                .client(ProjectTypeEnum.B)
                .build();
    }

    public Employee employeeBuilderAllocatedInAProject(){
        return  Employee.builder()
                .employeeType(EmployeeTypeEnum.Programmer1)
                .name("John Due")
                .cpf("089.876.123-02")
                .rg("12.456.754-4")
                .address("Street of River, 12 - New Your, USA.")
                .birthDay(LocalDate.of(1988, 01, 01))
                .email("jd@zallpy.com")
                .workedHours(10)
                .code("0001")
                .project(
                        Project.builder()
                                .id(1L)
                                .client(ProjectTypeEnum.A)
                                .build()
                )
                .build();
    }

    public Employee employeeBuilderAllocatedInBProject(){
        return  Employee.builder()
                .employeeType(EmployeeTypeEnum.Programmer2)
                .name("John Due")
                .cpf("089.876.123-02")
                .rg("12.456.754-4")
                .address("Street of River, 12 - New Your, USA.")
                .birthDay(LocalDate.of(1988, 01, 01))
                .email("jd@zallpy.com")
                .workedHours(10)
                .code("0001")
                .project(
                        Project.builder()
                                .id(1L)
                                .client(ProjectTypeEnum.B)
                                .build()
                )
                .build();
    }
}
