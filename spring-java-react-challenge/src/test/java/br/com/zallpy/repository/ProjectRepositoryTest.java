package br.com.zallpy.repository;

import br.com.zallpy.domain.Employee;
import br.com.zallpy.domain.Project;
import br.com.zallpy.enums.EmployeeTypeEnum;
import br.com.zallpy.enums.ProjectTypeEnum;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;

import java.time.LocalDate;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@DisplayName("ProjectRepository Unit Tests")
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class ProjectRepositoryTest {
    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Test
    @DisplayName("Test save project")
    public void givenGenericRepository_whenSaveProject_thenSuccessful(){
        Project project = projectABuilder();
        Project projectSaved = projectRepository.save(project);

        assertThat(projectSaved).isNotNull();
        assertThat(projectSaved.getClient()).isEqualTo(ProjectTypeEnum.A);
        assertThat(projectSaved.getId()).isEqualTo(1L);

    }

    @Test
    @DisplayName("Test update project")
    public void givenGenericRepository_whenUpdateProject_thenSuccessful(){
        Project project = projectABuilder();
        Project projectSaved = projectRepository.save(project);

        projectSaved.setClient(ProjectTypeEnum.B);
        projectSaved.setId(1L);

        Project updatedProject = projectRepository.save(projectSaved);

        assertThat(updatedProject).isNotNull();
        assertThat(projectSaved.getClient()).isEqualTo(ProjectTypeEnum.B);
        assertThat(projectSaved.getId()).isEqualTo(1L);

    }

    @Test
    @DisplayName("Test delete project")
    public void givenGenericRepository_whenDeleteProject_thenSuccessful(){
        Project project = projectABuilder();
        Project projectSaved = projectRepository.save(project);

        projectRepository.deleteById(projectSaved.getId());

        Optional<Project> byId = projectRepository.findById(projectSaved.getId());

        assertThat(byId).isEmpty();

    }


    @Test
    @DisplayName("Test get total of A project")
    public void givenGenericRepository_whenFindTotalHoursOfProjectA_thenSuccessful(){
        Employee employee = employeeBuilderAllocatedInAProject();

        Project project = projectABuilder();

        projectRepository.save(project);

        employeeRepository.save(employee);

        Integer totalOfWorkedHoursOnProjectA = projectRepository.getTotalOfWorkedHoursOnProjectA();

        assertThat(totalOfWorkedHoursOnProjectA).isEqualTo(10);

    }

    @Test
    @DisplayName("Test get total of A project without employee allocated")
    public void givenGenericRepository_whenFindTotalHoursOfProjectAndHaveNoEmployee_thenReturnZeroed(){

        Project project = projectABuilder();

        projectRepository.save(project);

        Integer totalOfWorkedHoursOnProjectA = projectRepository.getTotalOfWorkedHoursOnProjectA();

        assertThat(totalOfWorkedHoursOnProjectA).isNull();

    }

    @Test
    @DisplayName("Test get total of B project")
    public void givenGenericRepository_whenFindTotalHoursOfProjectB_thenSuccessful(){
        Employee employee = employeeBuilderAllocatedInBProject();

        Project project = projectBBuilder();

        projectRepository.save(project);

        employeeRepository.save(employee);

        Integer totalOfWorkedHoursOnProjectA = projectRepository.getTotalOfWorkedHoursOnProjectA();

        assertThat(totalOfWorkedHoursOnProjectA).isEqualTo(10);

    }

    @Test
    @DisplayName("Test get total of B project without employee allocated")
    public void givenGenericRepository_whenFindTotalHoursOfProjectBAndHaveNoEmployee_thenReturnNull(){

        Project project = projectBBuilder();

        projectRepository.save(project);

        Integer totalOfWorkedHoursOnProjectA = projectRepository.getTotalOfWorkedHoursOnProjectB();

        assertThat(totalOfWorkedHoursOnProjectA).isNull();

    }

    @Test
    @DisplayName("Test get total of all projects")
    public void givenGenericRepository_whenFindTotalHoursOfProjectAllProjects_thenSuccessful(){
        Employee employee1 = employeeBuilderAllocatedInAProject();
        Employee employee2 = employeeBuilderAllocatedInBProject();

        Project projectA = projectABuilder();
        Project projectB = projectBBuilder();

        projectRepository.save(projectA);
        projectRepository.save(projectB);

        employeeRepository.save(employee1);
        employeeRepository.save(employee2);

        Integer totalOfWorkedHoursOnProjectA = projectRepository.getTotalOfWorkedHoursInAllProjects();

        assertThat(totalOfWorkedHoursOnProjectA).isEqualTo(20);

    }

    @Test
    @DisplayName("Test get total of B project without employee allocated")
    public void givenGenericRepository_whenFindTotalHoursOfAllProjectsAndHaveNoEmployee_thenReturnNull(){

        Project project = projectABuilder();

        projectRepository.save(project);

        Integer totalOfWorkedHoursOnProjectA = projectRepository.getTotalOfWorkedHoursInAllProjects();

        assertThat(totalOfWorkedHoursOnProjectA).isNull();

    }

    public Project projectABuilder(){
        return Project.builder()
                .id(1L)
                .client(ProjectTypeEnum.A)
                .build();
    }

    public Project projectBBuilder(){
        return Project.builder()
                .id(2L)
                .client(ProjectTypeEnum.B)
                .build();
    }

    public Employee employeeBuilderAllocatedInAProject(){
        return  Employee.builder()
                .employeeType(EmployeeTypeEnum.Programmer1)
                .name("John Due")
                .cpf("089.876.123-02")
                .rg("12.456.754-4")
                .address("Street of River, 12 - New Your, USA.")
                .birthDay(LocalDate.of(1988, 01, 01))
                .email("jd@zallpy.com")
                .workedHours(10)
                .code("0001")
                .project(
                        Project.builder()
                                .id(1L)
                                .client(ProjectTypeEnum.A)
                                .build()
                )
                .build();
    }

    public Employee employeeBuilderAllocatedInBProject(){
        return  Employee.builder()
                .employeeType(EmployeeTypeEnum.Programmer2)
                .name("John Due")
                .cpf("089.876.123-02")
                .rg("12.456.754-4")
                .address("Street of River, 12 - New Your, USA.")
                .birthDay(LocalDate.of(1988, 01, 01))
                .email("jd@zallpy.com")
                .workedHours(10)
                .code("0001")
                .project(
                        Project.builder()
                                .id(1L)
                                .client(ProjectTypeEnum.B)
                                .build()
                )
                .build();
    }
}
