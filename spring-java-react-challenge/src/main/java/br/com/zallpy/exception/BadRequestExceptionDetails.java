package br.com.zallpy.exception;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class BadRequestExceptionDetails {
    private String title;
    private int code;
    private String details;
    private String timestamp;
}
