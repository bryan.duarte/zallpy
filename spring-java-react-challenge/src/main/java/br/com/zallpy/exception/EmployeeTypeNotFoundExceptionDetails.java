package br.com.zallpy.exception;

import lombok.Builder;
import lombok.Data;

import java.time.LocalTime;
@Data
@Builder
public class EmployeeTypeNotFoundExceptionDetails {
    private String title;
    private int status;
    private String details;
    private String timestamp;
    private LocalTime hour;
}
