package br.com.zallpy.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class EmployeeTypeNotFoundException extends RuntimeException {
    public EmployeeTypeNotFoundException(String message) {
        super(message);
    }
}
