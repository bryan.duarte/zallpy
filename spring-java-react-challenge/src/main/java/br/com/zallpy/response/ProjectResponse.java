package br.com.zallpy.response;

import br.com.zallpy.domain.Project;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class ProjectResponse {
   @JsonProperty("Projects")
   @Schema(description = "This is the list of projects")
   private List<Project> projectList;
}
