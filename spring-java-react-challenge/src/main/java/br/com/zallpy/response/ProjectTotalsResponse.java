package br.com.zallpy.response;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ProjectTotalsResponse {
    @Schema(description = "This the project name", example = "A or B")
    private String projectType;
    @Schema(description = "This is worked hours", example = "10")
    private Integer totalWorkedHours;
}
