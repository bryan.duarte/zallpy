package br.com.zallpy.response;

import br.com.zallpy.domain.Employee;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class EmployeeResponse {
    @Schema(description = "This is the total worked hours", example = "10")
    private Integer totalWorkedHours;
    @Schema(description = "This is the full data of employee in project")
    private List<Employee> projects;

}
