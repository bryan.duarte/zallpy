package br.com.zallpy.request;

import br.com.zallpy.domain.Project;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;
@Data
@Builder
public class EmployeeRequest {
    @JsonIgnore
    private Long id;
    protected String name;
    protected String cpf;
    protected String rg;
    protected String address;
    protected String email;
    protected LocalDate birthDay;
    private String code;
    private Integer workedHours;
    private String employeeType;
    private Project project;
}
