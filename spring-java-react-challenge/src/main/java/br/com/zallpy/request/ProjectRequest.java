package br.com.zallpy.request;

import br.com.zallpy.enums.ProjectTypeEnum;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@Builder
public class ProjectRequest {
    @JsonIgnore
    private Long id;

    @NotNull
    @NotEmpty(message = "filed client must be declared")
    @Schema(description = "This is the client type", example = "A", required = true)
    private String client;
}
