package br.com.zallpy.repository;

import br.com.zallpy.domain.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {
    @Query("SELECT e FROM Employee e where e.employeeType = 0 and e.code =:code")
    List<Employee> getProjectsOfProgrammer1(String code);

    @Query("SELECT e FROM Employee e where e.code =:code")
    List<Employee> getProjectsOfProgrammer2(String code);

    @Query("SELECT sum(e.workedHours) FROM Employee e where e.employeeType = 0 and e.code =:code")
    Integer getTotalWorkedHourOfProgrammer1(String code);

    @Query("SELECT sum(e.workedHours) FROM Employee e where e.employeeType = 1 and e.code =:code")
    Integer getTotalWorkedHourOfProgrammer2(String code);
}
