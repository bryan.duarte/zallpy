package br.com.zallpy.repository;

import br.com.zallpy.domain.Project;
import br.com.zallpy.enums.ProjectTypeEnum;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ProjectRepository extends JpaRepository<Project, Long> {
    @Query("SELECT sum(e.workedHours) FROM Employee e where e.project.id = 1")
    Integer getTotalOfWorkedHoursOnProjectA();

    @Query("SELECT sum(e.workedHours) FROM Employee e where e.project.id = 2")
    Integer getTotalOfWorkedHoursOnProjectB();

    @Query("SELECT sum(e.workedHours) FROM Employee e")
    Integer getTotalOfWorkedHoursInAllProjects();

}
