package br.com.zallpy.domain;

import br.com.zallpy.enums.EmployeeTypeEnum;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="EMPLOYEE")
public class Employee {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Long id;
    protected String name;
    protected String cpf;
    protected String rg;
    protected String address;
    protected String email;
    protected LocalDate birthDay;
    private String code;
    private Integer workedHours;
    private EmployeeTypeEnum employeeType;
    @ManyToOne
    @JoinColumn(name="project_id", referencedColumnName = "id", nullable=false)
    private Project project;
}
