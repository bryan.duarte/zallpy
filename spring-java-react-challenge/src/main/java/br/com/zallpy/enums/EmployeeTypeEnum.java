package br.com.zallpy.enums;

public enum EmployeeTypeEnum {
    Programmer1("Programmer 1"),
    Programmer2("Programmer 2");

    public String description;

     EmployeeTypeEnum(String description){
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
