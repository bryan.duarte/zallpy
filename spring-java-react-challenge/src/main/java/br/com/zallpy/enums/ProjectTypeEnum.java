package br.com.zallpy.enums;

public enum ProjectTypeEnum {
    A("Client A"),
    B("Client B");

    public String description;

    ProjectTypeEnum(String description){
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
