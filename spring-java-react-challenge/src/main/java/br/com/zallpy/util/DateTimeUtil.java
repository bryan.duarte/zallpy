package br.com.zallpy.util;

import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
@Component
public class DateTimeUtil {
    public String formatDateTimeToDatabaseFormat(LocalDateTime localDateTime){
        return DateTimeFormatter.ofPattern("dd-MM-yyyy").format(localDateTime);
    }
}
