package br.com.zallpy.security;

import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;

@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class InMemoryAuthentication extends WebSecurityConfigurerAdapter{

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .authorizeRequests()
                .anyRequest()
                .authenticated()
                .and()
                .httpBasic();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
       auth.inMemoryAuthentication()
               .withUser("programmer1@zallpy.com")
               .password(PasswordEncoderFactories.createDelegatingPasswordEncoder().encode("zallpy1"))
               .roles("PROGRAMMERONE")
               .and()
               .withUser("programmer2@zallpy.com")
               .password(PasswordEncoderFactories.createDelegatingPasswordEncoder().encode("zallpy2"))
               .roles("PROGRAMMERTWO")
               .and()
               .withUser("administrator@zallpy.com")
               .password(PasswordEncoderFactories.createDelegatingPasswordEncoder().encode("zallpyadmin"))
               .roles("ADMIN");

    }
}
