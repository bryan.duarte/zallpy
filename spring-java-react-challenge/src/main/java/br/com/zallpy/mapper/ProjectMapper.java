package br.com.zallpy.mapper;

import br.com.zallpy.domain.Project;
import br.com.zallpy.enums.ProjectTypeEnum;
import br.com.zallpy.request.ProjectRequest;
import org.springframework.stereotype.Component;

@Component
public class ProjectMapper {
    public Project projectBuilder(ProjectRequest projectRequest){
        return Project.builder().client(ProjectTypeEnum.valueOf(projectRequest.getClient())).build();
    }
}
