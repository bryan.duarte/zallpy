package br.com.zallpy.mapper;

import br.com.zallpy.domain.Employee;
import br.com.zallpy.enums.EmployeeTypeEnum;
import br.com.zallpy.exception.BusinessException;
import br.com.zallpy.exception.EmployeeTypeNotFoundException;
import br.com.zallpy.request.EmployeeRequest;
import org.springframework.stereotype.Component;

@Component
public class EmployeeMapper {
    public Employee employeeBuilder(EmployeeRequest employeeRequest){

        verifyIfEmployeeTypeExist(employeeRequest);

        return  Employee.builder()
                .name(employeeRequest.getName())
                .rg(employeeRequest.getRg())
                .cpf(employeeRequest.getCpf())
                .address(employeeRequest.getAddress())
                .email(employeeRequest.getEmail())
                .code(employeeRequest.getCode())
                .workedHours(employeeRequest.getWorkedHours())
                .birthDay(employeeRequest.getBirthDay())
                .employeeType(EmployeeTypeEnum.valueOf(employeeRequest.getEmployeeType()))
                .project(employeeRequest.getProject())
                .build();
    }

    public void verifyIfEmployeeTypeExist(EmployeeRequest employeeRequest){
        if(!employeeRequest.getEmployeeType().equals(EmployeeTypeEnum.Programmer1)
                || employeeRequest.getEmployeeType().equals(EmployeeTypeEnum.Programmer2)){
            throw new EmployeeTypeNotFoundException(String.format("This employee type not exist: '%s', see the documentation.",
                    employeeRequest.getEmployeeType()));
        }
    }
}
