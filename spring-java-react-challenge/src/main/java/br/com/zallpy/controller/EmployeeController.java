package br.com.zallpy.controller;

import br.com.zallpy.domain.Employee;
import br.com.zallpy.mapper.EmployeeMapper;
import br.com.zallpy.request.EmployeeRequest;
import br.com.zallpy.response.EmployeeResponse;
import br.com.zallpy.service.EmployeeService;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1/employees")
public class EmployeeController {

    private final EmployeeService employeeService;
    private final EmployeeMapper employeeMapper;

    public EmployeeController(EmployeeService employeeService, EmployeeMapper employeeMapper){
        this.employeeService = employeeService;
        this.employeeMapper = employeeMapper;
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode="200", description="Successful Operation"),
            @ApiResponse(responseCode = "200", description = "empty object in case don't have value {}")
    })
    @PreAuthorize("hasAnyRole('PROGRAMMERONE', 'PROGRAMMERTWO')")
    @PostMapping("/launch/hour/client/a")
    public ResponseEntity addWorkedHourOnClientA(@RequestBody EmployeeRequest employeeRequest){
        employeeService.addWorkedHourOnClientA(employeeMapper.employeeBuilder(employeeRequest));
        return new ResponseEntity(HttpStatus.OK);
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode="200", description="Successful Operation"),
            @ApiResponse(responseCode = "200", description = "empty object in case don't have value {}")
    })
    @PreAuthorize("hasRole('PROGRAMMERTWO')")
    @PostMapping("/launch/hour/b/client")
    public ResponseEntity addWorkedHourOnClientB(@RequestBody EmployeeRequest employeeRequest){
         employeeService.addWorkedHourOnClientB(employeeMapper.employeeBuilder(employeeRequest));
        return new ResponseEntity(HttpStatus.OK);
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode="200", description="Successful Operation"),
            @ApiResponse(responseCode = "200", description = "empty object in case don't have value {}")
    })
    @PreAuthorize("hasRole('PROGRAMMERONE')")
    @GetMapping("/list/project/programmer/one/{code}")
    public ResponseEntity<EmployeeResponse> getProjectsProgrammerOne(@PathVariable String code){
        return new ResponseEntity(employeeService.getProjectsOfProgrammer1(code), HttpStatus.OK);
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode="200", description="Successful Operation"),
            @ApiResponse(responseCode = "200", description = "empty object in case don't have value {}")
    })
    @PreAuthorize("hasRole('PROGRAMMERTWO')")
    @GetMapping("/list/project/programmer/two/{code}")
    public ResponseEntity<EmployeeResponse> getProjectsProgrammerTwo(@PathVariable String code){
        return new ResponseEntity(employeeService.getProjectsOfProgrammer2(code), HttpStatus.OK);
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode="200", description="Successful Operation"),
            @ApiResponse(responseCode = "200", description = "empty list in case don't have value []")
    })
    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping
    public ResponseEntity<List<Employee>> listEmployees(){
        return new ResponseEntity(employeeService.listAll(), HttpStatus.OK);
    }
}
