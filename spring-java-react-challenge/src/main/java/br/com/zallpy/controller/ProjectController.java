package br.com.zallpy.controller;

import br.com.zallpy.mapper.ProjectMapper;
import br.com.zallpy.request.ProjectRequest;
import br.com.zallpy.response.ProjectResponse;
import br.com.zallpy.response.ProjectTotalsResponse;
import br.com.zallpy.service.ProjectService;
import br.com.zallpy.util.DateTimeUtil;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDateTime;

@RestController
@RequestMapping("/v1/projects")
@Slf4j
public class ProjectController {

    private final ProjectService projectService;

    private final DateTimeUtil dateTimeUtil;
    private final ProjectMapper projectMapper;

    public ProjectController(DateTimeUtil dateTimeUtil, ProjectService projectService, ProjectMapper projectMapper){
        this.dateTimeUtil = dateTimeUtil;
        this.projectService = projectService;
        this.projectMapper = projectMapper;
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode="200", description="Successful Operation"),
            @ApiResponse(responseCode = "400", description = "Invalid Project")
            })
    @DeleteMapping(value = "/{id}")
    public ResponseEntity delete(@PathVariable Long id){
        log.info(dateTimeUtil.formatDateTimeToDatabaseFormat(LocalDateTime.now()));
        projectService.delete(id);
        return new ResponseEntity(HttpStatus.OK);
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode="200", description="Successful Operation"),
            @ApiResponse(responseCode = "400", description = "Invalid Project")
    })
    @PreAuthorize("hasRole('ADMIN')")
    @PutMapping
    public ResponseEntity update(@RequestBody ProjectRequest projectRequest){
        log.info(dateTimeUtil.formatDateTimeToDatabaseFormat(LocalDateTime.now()));
        projectService.update(projectRequest);
        return new ResponseEntity(HttpStatus.OK);
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode="200", description="With List"),
            @ApiResponse(responseCode = "200", description = "With empty list []")
    })
    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping
    public ResponseEntity<ProjectResponse> listAll(){
        log.info(dateTimeUtil.formatDateTimeToDatabaseFormat(LocalDateTime.now()));
        return ResponseEntity.ok(projectService.listAll());
    }


    @ApiResponses(value = {
            @ApiResponse(responseCode="200", description="With List"),
            @ApiResponse(responseCode = "400", description = "Invalid Project")
    })
    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/{id}")
    public ResponseEntity<ProjectResponse> findById(@PathVariable Long id){
        log.info(dateTimeUtil.formatDateTimeToDatabaseFormat(LocalDateTime.now()));
        return ResponseEntity.ok(projectService.findById(id));
    }


    @ApiResponses(value = {
            @ApiResponse(responseCode="200", description="With Project"),
            @ApiResponse(responseCode = "400", description = "field client must be declared")
    })
    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping
    public ResponseEntity<ProjectResponse> add(@Valid @RequestBody ProjectRequest projectRequest){
        log.info(dateTimeUtil.formatDateTimeToDatabaseFormat(LocalDateTime.now()));
        return new ResponseEntity(projectService.add(projectMapper.projectBuilder(projectRequest)), HttpStatus.CREATED);
    }


    @ApiResponses(value = {
            @ApiResponse(responseCode="200", description="Successful Operation")
    })
    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping("/active/registered/projects")
    public ResponseEntity activeRegisteredProjects(){
        log.info(dateTimeUtil.formatDateTimeToDatabaseFormat(LocalDateTime.now()));
       projectService.activeRegisteredProjects();
       return new ResponseEntity(HttpStatus.CREATED);
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode="200", description="Successful Operation"),
            @ApiResponse(responseCode = "200", description = "empty object in case don't have value {}")
    })
    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/total/hours/a/project")
    public ResponseEntity<ProjectTotalsResponse> getTotalOfWorkedHoursOnProjectA(){
        log.info(dateTimeUtil.formatDateTimeToDatabaseFormat(LocalDateTime.now()));
        return new ResponseEntity(projectService.getTotalOfWorkedHoursOnProjectA(), HttpStatus.OK);
    }


    @ApiResponses(value = {
            @ApiResponse(responseCode="200", description="Successful Operation"),
            @ApiResponse(responseCode = "200", description = "empty object in case don't have value {}")
    })
    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/total/hours/b/project")
    public ResponseEntity<ProjectTotalsResponse> getTotalOfWorkedHoursOnProjectB(){
        log.info(dateTimeUtil.formatDateTimeToDatabaseFormat(LocalDateTime.now()));
        return new ResponseEntity(projectService.getTotalOfWorkedHoursOnProjectB(), HttpStatus.OK);
    }


    @ApiResponses(value = {
            @ApiResponse(responseCode="200", description="Successful Operation"),
            @ApiResponse(responseCode = "200", description = "empty object in case don't have value {}")
    })
    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/total/hours")
    public ResponseEntity<ProjectTotalsResponse> getTotalOfWorkedHoursInAllProjects(){
        log.info(dateTimeUtil.formatDateTimeToDatabaseFormat(LocalDateTime.now()));
        return new ResponseEntity(projectService.getTotalOfWorkedHoursInAllProjects(), HttpStatus.OK);
    }
}
