package br.com.zallpy.handler;

import br.com.zallpy.exception.*;
import br.com.zallpy.util.DateTimeUtil;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

@ControllerAdvice
public class RestExceptionHandler {

    private final DateTimeUtil dateTimeUtil;

    public RestExceptionHandler(DateTimeUtil dateTimeUtil) {
        this.dateTimeUtil = dateTimeUtil;
    }

    @ExceptionHandler(BusinessException.class)
    public ResponseEntity handleBusinessException(BusinessException businessException) {
        BadRequestExceptionDetails exceptionDetail = BadRequestExceptionDetails.builder()
                .code(HttpStatus.BAD_REQUEST.value())
                .details(businessException.getMessage())
                .timestamp(dateTimeUtil.formatDateTimeToDatabaseFormat(LocalDateTime.now()))
                .title("Bad Request")
                .build();
        return new ResponseEntity(exceptionDetail, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity handleMethodArgumentNotValidException(MethodArgumentNotValidException methodArgumentNotValidException) {
        BadRequestExceptionDetails exceptionDetail = BadRequestExceptionDetails.builder()
                .code(HttpStatus.BAD_REQUEST.value())
                .timestamp(dateTimeUtil.formatDateTimeToDatabaseFormat(LocalDateTime.now()))
                .details(methodArgumentNotValidException.getMessage())
                .title(HttpStatus.BAD_REQUEST.getReasonPhrase())
                .build();
        return new ResponseEntity(exceptionDetail, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ProjectNotFoundException.class)
    public ResponseEntity handleProjectNotFoundException(ProjectNotFoundException projectNotFoundException) {

        ProjectNotFoundExceptionDetails notFoundExceptionDetails = ProjectNotFoundExceptionDetails.builder()
                .status(HttpStatus.BAD_REQUEST.value())
                .timestamp(dateTimeUtil.formatDateTimeToDatabaseFormat(LocalDateTime.now()))
                .details(projectNotFoundException.getMessage())
                .title(projectNotFoundException.getCause().getLocalizedMessage())
                .hour(LocalTime.now())
                .build();
        return new ResponseEntity(notFoundExceptionDetails, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(EmployeeTypeNotFoundException.class)
    public ResponseEntity handleEmployeeTypeNotFoundException(EmployeeTypeNotFoundException employeeTypeNotFoundException) {

        EmployeeTypeNotFoundExceptionDetails employeeTypeNotFoundExceptionDetails = EmployeeTypeNotFoundExceptionDetails.builder()
                .status(HttpStatus.BAD_REQUEST.value())
                .timestamp(DateTimeFormatter.ofPattern("dd-MM-yyyy").format(LocalDate.now()))
                .details(employeeTypeNotFoundException.getMessage())
                .title(employeeTypeNotFoundException.getCause().getLocalizedMessage())
                .hour(LocalTime.now())
                .build();
        return new ResponseEntity(employeeTypeNotFoundExceptionDetails, HttpStatus.BAD_REQUEST);
    }
}
