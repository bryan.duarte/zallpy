package br.com.zallpy.service;

import br.com.zallpy.domain.Employee;
import br.com.zallpy.enums.EmployeeTypeEnum;
import br.com.zallpy.exception.EmployeeTypeNotFoundException;
import br.com.zallpy.repository.EmployeeRepository;
import br.com.zallpy.response.EmployeeResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class EmployeeService {

    private final EmployeeRepository employeeRepository;

    public EmployeeService(EmployeeRepository employeeRepository){
        this.employeeRepository = employeeRepository;
    }

    public void addWorkedHourOnClientA(Employee employee){
        if(employee.getEmployeeType().equals(EmployeeTypeEnum.Programmer1) ||employee.getEmployeeType().equals(EmployeeTypeEnum.Programmer2)){
            throw new EmployeeTypeNotFoundException("This employee type not exist, see the documentation.");
        }
        employeeRepository.save(employee);
    }

    public void addWorkedHourOnClientB(Employee employee){
        employeeRepository.save(employee);
    }

    public EmployeeResponse getProjectsOfProgrammer1(String code){
        List<Employee> projectsOfProgrammer1 = employeeRepository.getProjectsOfProgrammer1(code);

        Integer totalWorkedHourOfProgrammer1 = employeeRepository.getTotalWorkedHourOfProgrammer1(code);

        return EmployeeResponse.builder()
                .projects(projectsOfProgrammer1)
                .totalWorkedHours(totalWorkedHourOfProgrammer1 == null ? 0 : totalWorkedHourOfProgrammer1)
                .build();
    }

    public EmployeeResponse getProjectsOfProgrammer2(String code){
        List<Employee> projectsOfProgrammer2 = employeeRepository.getProjectsOfProgrammer2(code);

        Integer totalWorkedHourOfProgrammer2 = employeeRepository.getTotalWorkedHourOfProgrammer2(code);

        return EmployeeResponse.builder()
                .projects(projectsOfProgrammer2)
                .totalWorkedHours(totalWorkedHourOfProgrammer2)
                .build();
    }

    public List<Employee> listAll(){
        return employeeRepository.findAll();
    }
}
