package br.com.zallpy.service;

import br.com.zallpy.domain.Project;
import br.com.zallpy.enums.ProjectTypeEnum;
import br.com.zallpy.exception.BusinessException;
import br.com.zallpy.exception.ProjectNotFoundException;
import br.com.zallpy.repository.EmployeeRepository;
import br.com.zallpy.repository.ProjectRepository;
import br.com.zallpy.request.ProjectRequest;
import br.com.zallpy.response.ProjectResponse;
import br.com.zallpy.response.ProjectTotalsResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Objects;
import java.util.Optional;

import static java.util.Arrays.asList;

@Service
@Slf4j
public class ProjectService {

    private final ProjectRepository projectRepository;
    private final EmployeeRepository employeeRepository;

    public ProjectService(ProjectRepository projectRepository, EmployeeRepository employeeRepository) {
        this.projectRepository = projectRepository;
        this.employeeRepository = employeeRepository;
    }

    public ProjectResponse listAll() {
        return ProjectResponse.builder().projectList(projectRepository.findAll()).build();
    }

    public ProjectResponse findById(Long id) {
        log.info(String.format("Searching for project for id: %s", id));
        Project project = projectRepository.findById(id).orElseThrow(() -> new BusinessException(
                String.format("Project not found for id %s", id)));
        return ProjectResponse.builder().projectList(asList(project)).build();
    }

    public ProjectResponse add(Project project) {
        ProjectResponse response = ProjectResponse.builder()
                .projectList(asList(projectRepository.save(project)))
                .build();
        return response;
    }

    @Transactional
    public void delete(Long id) {
        ProjectResponse foundProject = findById(id);

        if(Objects.isNull(foundProject)){
            throw new ProjectNotFoundException(String.format("Project not found for id '[%s]'", id));
        }

        projectRepository.deleteById(id);
    }

    @Transactional
    public void update(ProjectRequest projectRequest) {
        Optional<Project> foundProject = projectRepository.findById(projectRequest.getId());

        if(!projectRequest.getClient().equals(ProjectTypeEnum.A) || projectRequest.getClient().equals(ProjectTypeEnum.B)){
            throw new ProjectNotFoundException("Project type not declared, see documentation.");
        }

        if (Objects.nonNull(foundProject.get())) {
            foundProject.get().setClient(ProjectTypeEnum.valueOf(projectRequest.getClient()));
            projectRepository.save(foundProject.get());
        } else {
            throw new BusinessException(
                    String.format("Project not found for object %s", projectRequest));
        }
    }

    @Transactional
    public void activeRegisteredProjects(){
        Project clientA = Project.builder()
                .client(ProjectTypeEnum.A).build();
        projectRepository.save(clientA);

        Project clientB = Project.builder()
                .client(ProjectTypeEnum.B).build();
        projectRepository.save(clientB);
    }


    public ProjectTotalsResponse getTotalOfWorkedHoursOnProjectA(){
        Integer totalOfWorkedHoursOnProjectA = projectRepository.getTotalOfWorkedHoursOnProjectA();

        verifyIfTotalIsNull(totalOfWorkedHoursOnProjectA);

        return ProjectTotalsResponse.builder()
                .projectType(ProjectTypeEnum.A.getDescription())
                .totalWorkedHours(totalOfWorkedHoursOnProjectA).build();
    }

    public ProjectTotalsResponse getTotalOfWorkedHoursOnProjectB(){
        Integer totalOfWorkedHoursOnProjectB = projectRepository.getTotalOfWorkedHoursOnProjectB();

        verifyIfTotalIsNull(totalOfWorkedHoursOnProjectB);

        return ProjectTotalsResponse.builder()
                .projectType(ProjectTypeEnum.B.getDescription())
                .totalWorkedHours(totalOfWorkedHoursOnProjectB).build();
    }

    public ProjectTotalsResponse getTotalOfWorkedHoursInAllProjects(){
        Integer totalOfWorkedHoursInAllProjects = projectRepository.getTotalOfWorkedHoursInAllProjects();
        verifyIfTotalIsNull(totalOfWorkedHoursInAllProjects);

        return ProjectTotalsResponse.builder()
                .projectType(String.format("%s e %s",
                        ProjectTypeEnum.A.getDescription(),
                        ProjectTypeEnum.B.getDescription())
                )
                .totalWorkedHours(totalOfWorkedHoursInAllProjects)
                .build();
    }

    public void verifyIfTotalIsNull(Integer total){
        if(Objects.isNull(total)){
            throw new BusinessException("Have no employee allocated in this project.");
        }
    }
}
