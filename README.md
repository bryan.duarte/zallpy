# Zallpy

# Goal:

- Project created to make practice test of Java's knowledge and API creations using business logic of draft created by Zallpy on 
https://git.zallpylabs.com/challenges/spring-java-react-challenge.


# Database
This project have docker integration to easy connection with MySQL database, so before start project, install docker on machine if you don't have then run "docker-compose up" in src source root folder so after this  you can run project using Ap´plicationStart Spring Class or you can edit application.yml to configure database of your preference


# Basic Steps
For the appraiser's convenience, an endpoint was created which, after completing the step of uploading the project with bank integration, add two projects according to the interview, being project A and project B, the endpoint is "/v1/projects/active/registered/" projects run it with verb HTTP POST.

If you prefer to add projects manually, then use the url "/v1/projects" with verb HTTP POST following the specifications of the Swagger documentation.

Don't forget, all APIs are protected with Spring Security and Basic Auth, to access see the domain topic and when using postman suggestively, login with the appropriate credential.


# Authentication with Basic Auth - Registered Users

# Programmer1:
username: programmer1@zallpy.com
password: zallpy1

# APIs allowed for this user:

# Lauching hours in project A (see the documention of challenge, business rule topic)

url: server_address: server_port/v1/employees/launch/hour/client/a

HTTP METHOD: POST


# Visualization of projects and hours of project A (see the documention of challenge, topic business rule)

url: server_address: server_port/v1/employees/list/project/programmer/one/{code}

HTTP METHOD: GET


# Programmer2:
username: programmer2@zallpy.com
password: zallpy2

# APIs allowed for this user:

# Lauching hours in project A (see the documention of challenge, business rule topic)

url: server_address: server_port/v1/employees/launch/hour/client/a

HTTP METHOD: POST

# Lauching hours in project B (see the documention of challenge, business rule topic)

url: server_address: server_port/v1/employees/launch/hour/b/client

HTTP METHOD: POST

# Visualization of projects and hours of project A and B (see the documention of challenge, topic business rule)

url: server_address: server_port/v1/employees/list/project/programmer/two/{code}

HTTP METHOD: GET


# Administrator
username: administrator@zallpy.com
password: zallpyadmin

# APIs allowed for this user:

# Search projects by id:

url: server_address: server_port/v1/projects/{id}

HTTP METHOD: GET

# Deletion by id:

url: server_address: server_port/v1/projects/{id}

HTTP METHOD: DELETE

# Project update:

url: server_address: server_port/v1/projects/

HTTP METHOD: PUT

# List all projects:

url: server_address: server_port/v1/projects/

HTTP METHOD: GET

# Add a new project:

url: server_address: server_port/v1/projects/

HTTP METHOD: POST

# WARNING - Add Project A and B automatically based on the technical challenge proposed by zallpy to facilitate functional testing:

url: server_address: server_port/v1/projects/active/registered/projects

HTTP METHOD: POST

# Redeem total hours of project A:

url: server_address: server_port/v1/projects/total/hours/a/project

HTTP METHOD: GET

# Redeem total hours of project B:

url: server_address: server_port/v1/project /total/hours/b/project

HTTP METHOD: GET

# Redeem total hours for all projects:

url: server_address: server_port/v1/projects/total/hours

# Reedem all employees in all projects

url: server_address: server_port/v1/employees

HTTP METHOD: GET

# ER Diagram
You can also refer to the ER diagram attached to the same project.

# Complete API Documentation
To access APIs documentation after build project access localhost:8080/swagger-ui.html
or a document attached to the project in .json format.

# Final considerations:

1) You need to be authenticated with the admin user to open the documentation.

administrator@zallpy.com

2) All functional tests performed were done by Postman, including access with basic Auth in authorization, basic auth with email and password.













